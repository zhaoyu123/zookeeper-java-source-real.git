# zookeeper-java-source-real

#### 介绍

将zookeeper的源代码抽出来  慢慢看

#### 安装教程

里面是zookeeper的源码 使用最原始的lib下面的jar 其中ivy.xml文件中详细说明了依赖 我没有转成maven 的
直接下载 导入eclispe

#### 使用说明

使用的时候 将除lib文件夹外的 都进行设置 设为sourceFolder
（具体操作 选中后文件夹 右键 ->build path->use as a sourceFolder 

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)